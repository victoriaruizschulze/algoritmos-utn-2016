#include <iostream>
#include <conio.h> //para ingresar el char al final
using namespace std;

int v1[5] = {1,5,2,4,3};
int v2[5] = {2,1,3,4,5};
int v3[5] = {2,3,4,1,5};
int v4[5] = {2,3,5,4,1};
int v5[5] = {5,4,3,2,1};

void burbujeoOpt(int[], int);
void mostrarV(int[], int); 

int main() {

	burbujeoOpt(v1, 5);
	burbujeoOpt(v2, 5);
	burbujeoOpt(v3, 5);
	burbujeoOpt(v4, 5);
	burbujeoOpt(v5, 5);

	cout << "Presione cualquier tecla para cerrar esta ventana...";
	getch();
	return 0;

}

void burbujeoOpt(int v[], int len) {
	bool cambio;
	int p = 0; 
	int aux; 
	do {
		p++; 
		cambio = false; 
		int it = 0; 
		cout << endl << "Nuevo vector: "; 
		for (int i = 0; i < len - p; i++) {
			if (v[i] > v[i+1]) {
				aux = v[i];
				v[i] = v[i+1];
				v[i+1] = aux;

				cambio = true; 

			}
			it++;
			cout << it <<"-";
		}
	} while (cambio);
	mostrarV(v, len);
}

void mostrarV(int v[], int len) {
	cout << endl << "Vector ordenado: ";
	for (int i = 0; i < len; i++) {
		cout << v[i];
	}
}
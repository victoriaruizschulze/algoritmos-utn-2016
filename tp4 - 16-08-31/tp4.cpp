#include <iostream>
#include <stdio.h> //manejo de archivos (fopen, fclose, etc)
#include <conio.h> //para ingresar el char al final

using namespace std;

struct cliente {
	int numero;
	float deuda;
};
void mostrar_archivo(FILE*);
void busqueda_secuencial_y_actualizacion(int, FILE* &archivo);
void busqueda_binaria_y_actualizacion(int, FILE* &archivo);
int busqueda_binaria(FILE* &archivo, int nuevocliente, int desde, int hasta);
long cant_registros_archivo(FILE* &archivo); 

int main() {
	
	int nuevocliente;
	cliente cliente_leido;
	
	
	FILE* archivo = fopen("Clientes.bin", "rb+");

	//	El programa tiene que aceptar los pagos de los clientes y actualizar la deuda del mismo en el archivo. 
	
	//	Los pagos se ingresan por teclado. Por cada pago se ingresa n�mero de cliente, 
	cout << "Ingrese el numero de cliente (ingrese 0 para terminar): ";
	cin >> nuevocliente;
	
	while (nuevocliente!=0) {
		// 	el programa deber� informar si el cliente tiene deuda o no, y si la tiene informar el monto de la misma. 
		
		//busqueda_secuencial_y_actualizacion(nuevocliente, archivo);
		busqueda_binaria_y_actualizacion(nuevocliente, archivo);

		cout << endl << "Ingrese el numero de cliente (ingrese 0 para terminar): ";
		cin >> nuevocliente;
		//cout << endl <<nuevocliente;	
	}
	
	// 	Al finalizar el programa informar todos los clientes informando que no tienen deuda o el importe que adeudan seg�n corresponda
	mostrar_archivo(archivo);
	

	cout << "Presione cualquier tecla para cerrar esta ventana...";
	getch();
	return 0;
}

void mostrar_archivo(FILE* archivo) {
	cliente cliente_leido;
	fseek(archivo,0,0);
	fread(&cliente_leido,sizeof(cliente),1,archivo);
	while (!feof(archivo)) {
		
		cout<<cliente_leido.numero<< "   "<<cliente_leido.deuda<<endl;	
		fread(&cliente_leido,sizeof(cliente),1,archivo);
	}
}
void busqueda_secuencial_y_actualizacion(int nuevocliente, FILE* &archivo) {
	cliente cliente_leido;
	float pago;

	fseek(archivo,0,0);
	fread(&cliente_leido,sizeof(cliente),1,archivo);

	while (!feof(archivo) && cliente_leido.numero != nuevocliente) {
		fread(&cliente_leido,sizeof(cliente),1,archivo);	
	}

	if(cliente_leido.numero == nuevocliente ){
		
		cout<< endl << "La deuda del cliente es: "<<cliente_leido.deuda<<endl;
		
		// 	Si el cliente tiene deuda se ingresa el importe que abona, que puede ser el total que adeuda o parte. 
		if(cliente_leido.deuda > 0){
			cout<<"Ingrese monto a abonar: ";
			cin>>pago;
			
			//	Actualizar el archivo.
			cliente_leido.deuda = cliente_leido.deuda - pago;
			fseek(archivo, -1 * (int)sizeof(cliente), 1);
			fwrite(&cliente_leido, sizeof(cliente),1,archivo);
		}
	
	} else {
		cout<< endl << "No se encontro al cliente."<<endl;
	}	
}
void busqueda_binaria_y_actualizacion(int nuevocliente, FILE* &archivo) {
	cliente cliente_leido; 
	float pago;

	int posicion = busqueda_binaria(archivo, nuevocliente, 0, (int)cant_registros_archivo(archivo)-1);

	if (posicion >=0) {
		fseek(archivo, (int)sizeof(cliente) * posicion, SEEK_SET);
		fread(&cliente_leido,sizeof(cliente),1,archivo);

		cout<< "La deuda del cliente es: "<<cliente_leido.deuda<<endl;
		
		// 	Si el cliente tiene deuda se ingresa el importe que abona, que puede ser el total que adeuda o parte. 
		if(cliente_leido.deuda > 0){
			cout<<"\tIngrese monto a abonar: ";
			cin>>pago;
			
			//	Actualizar el archivo.
			cliente_leido.deuda = cliente_leido.deuda - pago;
			fseek(archivo, -1 * (int)sizeof(cliente), SEEK_CUR);
			fwrite(&cliente_leido, sizeof(cliente),1,archivo);
		}	
	} else {
		cout << "No se encontro al cliente"<<endl<<endl;
	}
}
int busqueda_binaria(FILE* &archivo, int nuevocliente, int desde, int hasta) {
	cliente cliente_leido; 

	if (desde <= hasta) {
		int medio = (hasta - desde) / 2 + desde;
		fseek(archivo, medio * (int)sizeof(cliente), SEEK_SET); //Me posiciono en el medio del archivo
		fread(&cliente_leido,sizeof(cliente),1,archivo); //Leo el registro del medio

		if (nuevocliente == cliente_leido.numero) {
			return medio;
		} else {
			if (nuevocliente > cliente_leido.numero){
				busqueda_binaria(archivo, nuevocliente, medio+1, hasta);
			} else {
				busqueda_binaria(archivo, nuevocliente, desde, medio-1);
			}
		}
		
	} else {
		return -1;
	}
}
long cant_registros_archivo(FILE* &archivo) {
	long posic_original = ftell(archivo);

	fseek(archivo, 0, SEEK_END);
	long ultimo = ftell(archivo);

	fseek(archivo, posic_original, SEEK_SET); //Vuelvo a la posicion original

	return ultimo/sizeof(cliente);
}
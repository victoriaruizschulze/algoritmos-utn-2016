#include <iostream>
#include <stdlib.h> //rand, srand, NULL
#include <stdio.h> //manejo de archivos (fopen, fclose, etc)
#include <string.h> //manejo de strings
#include <time.h> //time
#include <conio.h> //para ingresar el char al final
using namespace std;

// ---------- CONSIGNA ---------- //
/* 
	En una pequeña localidad se va a instalar un servicio de contratación de traslado de
	personas provisto por autos particulares. En una primera etapa se admiten hasta 100 autos.
	Se quiere desarrollar un programa para el registro de los autos que cumplirán con el
	servicio.

	Para hacer el registro de los autos a registrar se ingresan por teclado los siguientes datos por
	cada auto:

	- Patente
	- Marca
	- Apellido y nombre del dueño

	El programa debe:

	1. Generar un archivo de datos almacenando los autos ordenado por patente.
	2. Mostrar por cada marca de auto la cantidad de unidades registradas de la misma, ordenado por cantidad de autos.

*/

struct registroAuto {
	char marca[15];
	char patente[25]; 
	char nom[25];
	char apellido[25];
}; 
struct contadorMarcas {
	int cantidad;
	char marca[15];
};

const int maximo_autos = 100;
int cantidad_autos = maximo_autos;
int metodo_de_ingreso;
FILE* archivo;
bool ver_variables_testeo = 0;

registroAuto arrayAOrdenar[100];
contadorMarcas arrayOrdenado[100];

void ingresoManual(); 
void generacionAleatoria();
void revisionIngresos();
void contarPorMarca();
void burbujeoOpt(contadorMarcas vec[], int cant);
void ordenarPorPatente(registroAuto vec[], int cant);
void deArrayAArchivo(registroAuto vec[], int cant);

int main() {
	srand(time(0));
	archivo = fopen("autos.dat","w+b");
	cout << "Como desea ingresar los autos?"<<endl;
	cout << "1. Ingreso manual. Tipear datos de cada auto."<<endl;
	cout << "2. Generacion aleatoria de "<< maximo_autos <<" registros (para testear programa rapidamente)."<<endl<<endl;

	cout << "Su eleccion: ";
	cin >> metodo_de_ingreso; 
	while (metodo_de_ingreso != 1 && metodo_de_ingreso != 2) {
		cout << "Debe ingresar una opcion valida: ";
		cin >> metodo_de_ingreso;
	}
    cout << endl;

    switch (metodo_de_ingreso) {
    	case 1: 
    		ingresoManual();
    		break;
    	case 2: 
    		generacionAleatoria();
    		break;
    	default: 
    		cout << "Este programa no anda." << endl << endl;
    		break;
    }

    //Verificación de variables: 
    if (ver_variables_testeo) { 
		cout << "Metodo de ingreso: " << metodo_de_ingreso << endl; //Para testear
		cout << "Cantidad de autos: " << cantidad_autos << endl; //Para testear
    }
	
	fclose(archivo);

	int verListado;
	cout << "Ingrese 1 si desea ver los valores ingresados: ";
	cin >> verListado;
	cout << endl;

	if (verListado == 1) {
		revisionIngresos();	
	}
	cout <<endl<<endl;
	contarPorMarca();

	//Esperar a que se apriete una tecla para cerrar la ventana
	cout << "Presione cualquier tecla para cerrar esta ventana...";
	getch();
	return 0;
	
}
void ingresoManual() {
	string r_marca, r_nombre, r_apellido, r_patente;
	registroAuto r_auto;

	cout << "Cuantos autos desea ingresar? El maximo es "<<maximo_autos<<"." << endl;
	cin >> cantidad_autos; 
	while (cantidad_autos > maximo_autos || cantidad_autos < 0) {
		cout << "Debe ingresar un valor mayor que 0 y menor o igual que "<<maximo_autos<<"." << endl;
		cin >> cantidad_autos;
	}
    cout << endl;

    for (int i = 0; i < cantidad_autos; i++) {
    	cout << "REGISTRO "<< i+1 <<": "<<endl;
    	cout << "Marca: "; 
    	cin >> r_marca;

    	cout << "Patente: "; 
    	cin >> r_patente;

    	cout << "Nombre: "; 
    	cin >> r_nombre;

    	cout << "Apellido: "; 
    	cin >> r_apellido;
    	cout << endl;

    	//Guardar cada auto en un array que falta ordenar
    	strcpy(arrayAOrdenar[i].patente,r_patente.c_str());
	    strcpy(arrayAOrdenar[i].marca,r_marca.c_str());	
	    strcpy(arrayAOrdenar[i].apellido,r_apellido.c_str());
		strcpy(arrayAOrdenar[i].nom,r_nombre.c_str());

    }
    if (ver_variables_testeo) {
	    for (int ord = 0; ord < cantidad_autos; ord++) {
	    	cout << arrayAOrdenar[ord].patente << endl;
	    }
	}
    cout << "Ha terminado de ingresar el listado de autos. ";

    //Ordenar Array
    ordenarPorPatente(arrayAOrdenar, cantidad_autos);
    deArrayAArchivo(arrayAOrdenar, cantidad_autos);

}
void generacionAleatoria() {
	string r_marca, r_nom, r_apellido; 
	char r_patente[6];

	int num_rand; 
	registroAuto r_auto;

	string nombres[16] = {
		"Juan", "Pedro", "Roberto", "Miguel", "Guillermo", "Emilio", "Roque", "Gustavo",
		"Ana", "Paula", "Patricia", "Virginia", "Antonella", "Clara", "Valeria", "Cecilia"
	} ; 
	string apellidos[17] = {
		"Francella", "Lanzani", "Echarri", "Suar", "Sbaraglia", "Arana", "Carnaghi", "Laport",
		"Aleandro", "Silveyra", "Blum", "Legrand", "Del Boca", "Lopilato", "Cid", "Fonzi", "Peterson"
	};
	string marcas[12] = {
		"Volvo", "Renault", "Audi", "Ford", "Fiat", "Chevrolet", "Nissan", "Volkswagen", "Mercedes Benz", "Rolls Royce", "Delorean", "Aston Martin"
	};

	char letras_patentes[] = "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
	char numeros_patentes[] = "0123456789"; 

	for (int i = 0; i < cantidad_autos; i++) {
		r_marca = marcas[rand() % (sizeof(marcas)/sizeof(marcas[0]))];
    	r_nom = nombres[rand() % (sizeof(nombres)/sizeof(nombres[0]))];
    	r_apellido = apellidos[rand() % (sizeof(apellidos)/sizeof(apellidos[0]))];

    	for(int m = 0; m < 3; ++m) {
	    	r_patente[m] = letras_patentes[rand() % (sizeof(letras_patentes)/sizeof(letras_patentes[0])-1)];
	    }	
	    for(int n = 3; n < 6; n++) {
	    	r_patente[n] = numeros_patentes[rand() % (sizeof(numeros_patentes)/sizeof(numeros_patentes[0])-1)];
	    }
	    r_patente[7] = '\0';
	    	    
	   	strcpy(arrayAOrdenar[i].patente,r_patente);
	    strcpy(arrayAOrdenar[i].marca,r_marca.c_str());	
	    strcpy(arrayAOrdenar[i].apellido,r_apellido.c_str());
		strcpy(arrayAOrdenar[i].nom,r_nom.c_str());
	    
	    if (ver_variables_testeo) { 
	    	cout << (i+1) << ") " << r_auto.marca << " - " << r_auto.patente << " - " << r_auto.nom << " " << r_auto.apellido << endl; //Para testear
		}
	}
	cout << "Se ha generado aleatoriamente un listado de "<<cantidad_autos<<" autos. ";
	ordenarPorPatente(arrayAOrdenar, cantidad_autos);
	deArrayAArchivo(arrayAOrdenar, cantidad_autos);
}
void revisionIngresos() {

	FILE* archivoLeer = fopen("autos.dat","r+b");
	registroAuto r_leido; 
	int i = 0;
	fread (&r_leido, sizeof(registroAuto), 1, archivoLeer);
	while (!feof(archivoLeer)) {
		i++;
		cout << i << ") "<< r_leido.marca << " - " << r_leido.patente << " - " << r_leido.nom << " " << r_leido.apellido << endl; //Para testear
		fread (&r_leido, sizeof(registroAuto), 1, archivoLeer);
	}

	fclose(archivoLeer);
};
void contarPorMarca() {
	FILE* archivoLeer = fopen("autos.dat","r+b");
	registroAuto r_leido; 
	int aux = 0;
	int flag_existe = 0;
	int posicion_existente;

	fread (&r_leido, sizeof(registroAuto), 1, archivoLeer);
	
	while (!feof(archivoLeer)) {

		for (int a = 0; a<aux; a++) { //ya existe la marca? 
			if (string(arrayOrdenado[a].marca) == string(r_leido.marca)) { 
				flag_existe = 1; 
				posicion_existente = a;
				break;
			} else {
				flag_existe = 0;
			}
		}	
		if (flag_existe == 1) { //Existe: sumar 1
			arrayOrdenado[posicion_existente].cantidad++;
		} else { //no existe: agregar marca e inicializar contador en 1, aux++
			strcpy(arrayOrdenado[aux].marca,r_leido.marca);
			arrayOrdenado[aux].cantidad = 1;
			aux++;
		}

		
		fread (&r_leido, sizeof(registroAuto), 1, archivoLeer);
	}

	if (ver_variables_testeo) {
		cout << "Cantidad de autos por marca (lista desordenada): "<<endl;
		cout << "------------------------------------------------"<<endl;
		for (int q = 0; q < aux; q++) {
			cout << arrayOrdenado[q].marca << ": "<< arrayOrdenado[q].cantidad << endl;
			}
		cout <<endl;	
	}
	

	burbujeoOpt(arrayOrdenado, aux); 
	cout << "Cantidad de autos por marca: "<<endl;
	cout << "----------------------------"<<endl;
	for (int q = 0; q < aux; q++) {
		cout << arrayOrdenado[q].marca << ": "<< arrayOrdenado[q].cantidad << endl;
	}	cout <<endl;
	fclose(archivoLeer);
}
void ordenarPorPatente(registroAuto vec[], int cant) {	
	registroAuto aux;
    int q = 0;
    bool cambio;

    do {
        q++;
        cambio = false;

        for (int r = 0; r < cant - q; r++) {
             if (strcmp(vec[r].patente, vec[r+1].patente) > 0 ) {
                
                strcpy(aux.marca, vec[r].marca);
                strcpy(aux.patente, vec[r].patente);
                strcpy(aux.nom, vec[r].nom);
                strcpy(aux.apellido, vec[r].apellido);

                strcpy(vec[r].marca, vec[r+1].marca);
                strcpy(vec[r].patente, vec[r+1].patente);
                strcpy(vec[r].nom, vec[r+1].nom);
                strcpy(vec[r].apellido, vec[r+1].apellido);

                strcpy(vec[r+1].marca, aux.marca);
                strcpy(vec[r+1].patente, aux.patente);
                strcpy(vec[r+1].nom, aux.nom);
                strcpy(vec[r+1].apellido, aux.apellido);

                cambio = true;
            }
        }
    } while (cambio == true);
}
void deArrayAArchivo(registroAuto vec[], int cant) {
	for (int i = 0; i < cant; i++) {
		fwrite(&vec[i],sizeof(registroAuto),1,archivo);	
	}
}
void burbujeoOpt(contadorMarcas vec[], int cant) {
    contadorMarcas aux;
    int q = 0;
    bool cambio;

    do {
        q++;
        cambio = false;

        for (int r = 0; r <= cant - q; r++) { 
             if (vec[r].cantidad < vec[r+1].cantidad ) {
                aux.cantidad = vec[r].cantidad;
                strcpy(aux.marca, vec[r].marca);

                vec[r].cantidad = vec[r+1].cantidad;
                strcpy(vec[r].marca, vec[r+1].marca);

				vec[r+1].cantidad = aux.cantidad;
                strcpy(vec[r+1].marca, aux.marca);

                cambio = true;
            }
        }
    } while (cambio == true);
}
#include <iostream>
#include <time.h>
#include <conio.h> //para ingresar el char al final

using namespace std;

void mostrar_linea(int);
int producto(int, unsigned int);
int potencia(int, unsigned int);

int main() {

	int opcion_menu;
	int cant; 
	
	do {
		cout << "\n\nBUEN DIA! QUE DESEA HACER? \n";
		cout << "--------------------------------------------------------------------------\n";
		cout << "   1) Mostrar n veces una linea de texto.\n";
		cout << "   2) Obtener el producto de 2 numeros enteros positivos (a y b) calculado como la sumatoria de A, B veces.\n";
	    cout << "   3) Obtener A^n como el producto de A, N veces.\n";
	    //cout << "   4) Informar por zona pedidos de reparacion listos para atender.\n";
	    cout << "   0) Finalizar el programa\n";
		cout << "--------------------------------------------------------------------------\n";
		cout << "Su eleccion: ";
		cin >> opcion_menu; //Validar?
		cout << endl;


		switch(opcion_menu) {
			case 1:
				cout << "\nCuantas veces? ";
				do {
					cin >> cant; 
				} while (cant < 0); 
				mostrar_linea(cant);
				break;
			case 2: 
				unsigned int a; unsigned int b; 
				cout << "\nIngrese A: ";
				do {
					cin >> a; 
				} while (a < 0); 					
				cout << "Ingrese b: ";
				do {
					cin >> b; 
				} while (b < 0); 
				cout << "\nResultado: "<<a<<"*"<<b<<" = " << producto(a, b);
				break;
			case 3: 
				int c; unsigned int n; 
				cout << "\nIngrese A: ";
				cin >> c; 
				cout << "Ingrese n: ";
				do {
					cin >> n; 
				} while (n < 0); 
				cout << "\nResultado: "<<c<<"^"<<n<<" = " << potencia(c, n);
				break;

			case 0:
				cout << "Muchas gracias por utilizar este programa. Adios!" << endl;
				break;
			default:
				cout << "Opcion no valida.\n";
				break;
		}
		
	} while (opcion_menu != 0);
	    
    getch();
    return 0;
}

void mostrar_linea(int cant) {
	if (cant > 0) {
		cout << cant << ". Hola que tal? \n";
		mostrar_linea(cant-1);
	} else {
		return;
	}
}
int producto (int a, unsigned int b) {
	if (b == 0) {
		return 0;
	} else {
		return a + producto(a, (b-1));
	}
}
int potencia(int a, unsigned int n) {
	if (n > 0) {
		return a * potencia(a, (n-1));
	} else {
		return 1;
	}
}

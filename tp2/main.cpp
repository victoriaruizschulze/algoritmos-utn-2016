#include <iostream>
#include <stdio.h>
#include <string.h>

using namespace std;

struct Material
{
    int codigo;
    char descripcion[26];
};
struct Pedido
{
    int deposito;
	int material;
    int cantidad;
};
int mat[10][100]={{0}}; // Inicializar variable con valor 0 en cada posicion
Material materiales[100];
int sumCol[100] = {0};
int sumFila[10] = {0};
int cantMateriales = 0;

void generar();
void mostrar();
void generarVectorMateriales();
void ingresarPedidos();
void mostrarMatriz();
void ordenarMateriales(int);
int columnaMaterial(int);
void mostrarPedidosDepositos();
void mostrarPedidosMateriales();
void mostrarDepositosVacios();
int columnaMaterialBinaria(int, int,int);

int main()
{
    //generar();
    //mostrar();
    //mostrarMatriz();
 
    generarVectorMateriales();
	ingresarPedidos();
	mostrarPedidosDepositos(); //incluye total por deposito
	mostrarPedidosMateriales(); //incluye maximo
	mostrarDepositosVacios();
}

void generar()
{
    FILE * arch=fopen("Materiales.bin","wb");
    Material reg;
    cout<<"Ingrese codigo del material (0 para finalizar)  ";
    cin>>reg.codigo;
    while(reg.codigo!=0)
    {
        cout<<"Descripcion del material ";
        cin>>reg.descripcion;
        fwrite(&reg,sizeof(Material),1,arch);
        cout<<"Ingrese codigo del material (0 para finalizar)  ";
        cin>>reg.codigo;
    }
    fclose(arch);
}

void mostrar()
{
    FILE * arch=fopen("Materiales.bin","rb");
    Material reg;
    fread(&reg,sizeof(Material),1,arch);
    while(!feof(arch))
    {
        cout<<"Codigo "<<reg.codigo;
        cout<<"  Descripcion "<<reg.descripcion<<endl;
        fread(&reg,sizeof(Material),1,arch);

    }
    fclose(arch);
}
void generarVectorMateriales(){
	FILE * arch=fopen("Materiales.bin","rb");
    Material reg;
    fread(&reg,sizeof(Material),1,arch);
    int i = 0; 
    while(!feof(arch))
    {
    	materiales[i] = reg;
    	/*materiales[i].codigo = reg.codigo;
        strcpy(materiales[i].descripcion, reg.descripcion);*/
        i++; //Usar para ordenar!
        fread(&reg,sizeof(Material),1,arch);
    }
    fclose(arch);
    cantMateriales = i;

    ordenarMateriales(cantMateriales);
    //Ver matriz
    for(int j=0; j<100; j++) {
    	if (materiales[j].codigo !=0) {
			cout<<materiales[j].codigo<<"\t"<<materiales[j].descripcion;
			cout<<endl;
			
		}
    	
	}
	
	
}
void mostrarMatriz() {
	//Muestra todos los valores de la matriz de pedidos en pantalla
	for (int i=0; i<10;i++) {
		for (int j=0; j<100;j++){
			cout<<mat[i][j]<<' ';
		}
		cout<<endl;
	}
}
void ordenarMateriales(int cant) {
    Material aux;
    int q = 0;
    bool cambio;

    do {
        q++;
        cambio = false;

        for (int r = 0; r < cant - q; r++) { 
        	 /*if (materiales[r].codigo == 0 || materiales[r+1].codigo == 0 ) {
        	 	//No ordenar valores vacios de la matriz
        	 	break;
			 }*/
             if (materiales[r].codigo > materiales[r+1].codigo  ) {
                aux.codigo = materiales[r].codigo;
                strcpy(aux.descripcion, materiales[r].descripcion);

                materiales[r].codigo = materiales[r+1].codigo;
                strcpy(materiales[r].descripcion, materiales[r+1].descripcion);

				materiales[r+1].codigo = aux.codigo;
                strcpy(materiales[r+1].descripcion, aux.descripcion);
				
                cambio = true;
            } 
        }
    } while (cambio == true);

}
void ingresarPedidos(){
	
	Pedido pedido;
	int fin_ingreso = 0;
	int columna_material; 
	
	cout << endl << endl <<"Teniendo en cuenta la lista de materiales existentes, ingrese su(s) pedido(s). Ingrese deposito 0 para finalizar el ingreso."<<endl;
	
	while (fin_ingreso == 0) {
		cout << endl << "PEDIDO:";
		cout << endl << "Numero de deposito: ";
		cin >> pedido.deposito;
		while (pedido.deposito > 10 || pedido.deposito < 0) {
			cout << "Ingrese un numero de deposito valido: ";
			cin >> pedido.deposito;
		}
		if (pedido.deposito != 0) {
			
			cout << "Codigo de material: ";
			cin >> pedido.material; 
			columna_material = columnaMaterialBinaria(pedido.material,0,cantMateriales-1);
			while (columna_material == -1) {
				cout << "Ingrese un codigo de material valido: ";
				cin >> pedido.material; 
				columna_material = columnaMaterialBinaria(pedido.material,0, cantMateriales-1);
			}

			
			cout << "Cantidad: ";
			cin >> pedido.cantidad;
			while (pedido.cantidad <= 0) {
				cout << "Ingrese una cantidad valida: ";
				cin >> pedido.cantidad;
			}
			
			mat[pedido.deposito - 1][columna_material] += pedido.cantidad;
			
		} else {
			fin_ingreso = 1;
		}
		
		//Guardar pedido en matriz
		//obtener columna correspondiente al material
		
		
	}
}
int columnaMaterial(int codigo){
	int columna = -1;
	for (int i = 0; i<100; i++) {
		if (materiales[i].codigo == codigo) {
			columna = i;
			break; //reemplazar por while
		}
	}
	return columna;
}
int columnaMaterialBinaria(int codigo, int desde, int hasta){
	int columna = -1;
	//cout <<endl<<desde << "-"<<hasta<<endl;
	if (desde <= hasta) {
		
		int medio = (hasta - desde) / 2 + desde;
		//cout <<"medio: "<<medio<<endl;
		
		if (codigo == materiales[medio].codigo) {
			//cout <<"Es igual"<<endl;
			return medio;
		} else {
			//cout <<"no es igual"<<endl;
			if (codigo > materiales[medio].codigo){
				columna = columnaMaterialBinaria(codigo, medio+1, hasta);
			} else {
				//cout <<"primer mitad";
				columnaMaterialBinaria(codigo, desde, medio-1);
			}
		}
		
	} else {
		return -1;
	}
}

void mostrarPedidosDepositos() {
	cout <<endl<<endl;
	int hayPedidos = 0;
	for (int i = 0; i < 10; i++){
		hayPedidos = 0;
		for (int j = 0; j < 100; j++){
			if (mat[i][j] > 0) {
				if (hayPedidos == 0) {
					cout << endl <<"DEPOSITO "<<i+1<<":"<<endl;
					hayPedidos++;
				}
				sumFila[i] += mat[i][j];
				cout << materiales[j].codigo << " "<<materiales[j].descripcion <<": "<< mat[i][j]<<endl;
			}
		}
	}
}
void mostrarPedidosMateriales() {
	cout <<endl<<endl<<"PEDIDOS ORDENADOS POR MATERIALES:"<<endl;
	for (int j = 0; j < 100; j++) {
		sumCol[j] = 0;
		if (materiales[j].codigo > 0) {
			for (int i = 0; i<10; i++) {
				sumCol[j] += mat[i][j];
			}
			if (sumCol[j] > 0) {
				cout << materiales[j].codigo << " "<<materiales[j].descripcion<<": "<<sumCol[j]<<endl;
			}
			
		} else {
			break; //No analizar las posiciones vacias del vector de materiales
		}
	}
	int maxCol = -1;
	int valMax = 0;
	for (int k = 0; k < 100; k++){
		if (sumCol[k]>valMax) {
			valMax = sumCol[k];
			maxCol = k;
		}
	}
	if (maxCol == -1) {
		cout << "No se han registrado pedidos de ningun material."<<endl;
	} else {
		cout <<"El material mas solicitado fue el "<<materiales[maxCol].descripcion<<" (cod. "<<materiales[maxCol].codigo<<"). Se solicitaron "<<valMax<<" unidades."<<endl;
	}
}

void mostrarDepositosVacios(){
	cout <<endl<<endl<<"Los depositos que no recibieron materiales fueron: "<<endl;
	for (int i = 0; i < 10; i++) {
		if (sumFila[i] == 0) {
			cout <<i+1<<endl;
		}
	}
}

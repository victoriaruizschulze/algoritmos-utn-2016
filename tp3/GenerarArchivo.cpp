#include <iostream>
#include <stdio.h>

using namespace std;

struct Dato
{
    int legajo;
    int nota;
};
Dato vectorExamenes[50];
	
void generar(char []);
void mostrar(char []);
void guardarPromedioTP();
void ordenarDatos(char [], Dato []);
void mostrarNotaFinal();

int main()
{
	for (int i =0; i<50; i++) {
		vectorExamenes[i].legajo = 0;
	}
    /*cout<<"Generar archivo Examenes.bin"<<endl;
    generar("Examenes.bin");*/
    cout<<"Mostrar archivo Examenes.bin"<<endl;
    mostrar("Examenes.bin");
    /*cout<<"Generar archivo TPs.bin"<<endl;
    generar("TPs.bin");*/
    cout<<"Mostrar archivo TPs.bin"<<endl;
    mostrar("TPs.bin");
    guardarPromedioTP();
    
	cout<<"Mostrar archivo PromedioTPs.bin"<<endl;
    mostrar("PromedioTPs.bin");
    
	ordenarDatos("Examenes.bin", vectorExamenes);
    
    mostrarNotaFinal();

}
void mostrarNotaFinal(){
	FILE * a_promedios =fopen("PromedioTPs.bin","rb");
	Dato tps;
	int cont = 0;
	
	cout<<"Mostrar notas finales"<<endl;
	
	fread(&tps,sizeof(Dato),1,a_promedios);
	while (!feof(a_promedios) && cont < 50 && vectorExamenes[cont].legajo > 0) {
		if (tps.legajo < vectorExamenes[cont].legajo) {
			cout << "Legajo " << tps.legajo << "  Nota " << tps.nota << endl;
			fread(&tps,sizeof(Dato),1,a_promedios);
		} else {
			if (tps.legajo == vectorExamenes[cont].legajo) {
				cout << "Legajo " << tps.legajo << "  Nota " ;
				if (tps.nota >= vectorExamenes[cont].nota) {
					cout << tps.nota << endl;
				} else {
					cout << vectorExamenes[cont].nota << endl;
				}
				fread(&tps,sizeof(Dato),1,a_promedios);
				cont++;
			} else {
				cout << "Legajo " << vectorExamenes[cont].legajo << "  Nota " << vectorExamenes[cont].nota << endl;
				cont++;
			}
		}
	}
	while (!feof(a_promedios)) {
		cout << tps.legajo << ": " << tps.nota << endl;
		fread(&tps,sizeof(Dato),1,a_promedios);
	}
	while (cont < 50 && vectorExamenes[cont].legajo > 0) {
		cout << "Legajo " << vectorExamenes[cont].legajo << "  Nota " << vectorExamenes[cont].nota << endl;
		cont++;
	}
	fclose(a_promedios);
}

void guardarPromedioTP() {
	FILE * a_tps =fopen("TPs.bin","rb");
	FILE * a_promedios =fopen("PromedioTPs.bin","wb");
	
	Dato tp, promedio;
	int leg;
	int cont, acum; 
	
	fread(&tp,sizeof(Dato),1,a_tps);
	
	while(!feof(a_tps)) {
		
		leg = tp.legajo;
		acum = 0; 
		cont = 0; 
		
		do {
			acum += tp.nota;
			cont++;
			fread(&tp,sizeof(Dato),1,a_tps);
		} while(!feof (a_tps) && tp.legajo == leg);
		
		promedio.legajo = leg; 
		promedio.nota = acum/cont;
		fwrite(&promedio,sizeof(Dato),1,a_promedios);
		
	}
	fclose(a_tps);
	fclose(a_promedios);
}
void generar(char nomArch[])
{
    FILE * arch=fopen(nomArch,"wb");
    Dato reg;
    cout<<"Ingrese legajo (0 para finalizar)  ";
    cin>>reg.legajo;
    while(reg.legajo!=0)
    {
        cout<<"Nota ";
        cin>>reg.nota;
        fwrite(&reg,sizeof(Dato),1,arch);
        cout<<"Ingrese legajo (0 para finalizar)  ";
        cin>>reg.legajo;
    }
    fclose(arch);
}

void ordenarDatos(char nomArch[], Dato vector[]) {

	//1. Paso examenes a vector
	FILE * a_examenes = fopen(nomArch,"rb");
    Dato examen;
    int cant = 0;
    
    fread(&examen, sizeof(Dato),1,a_examenes);
    
	while ( !feof(a_examenes) ) {
    	vector[cant] = examen;
    	fread(&examen, sizeof(Dato),1,a_examenes);
    	cant++;
	}
    fclose(a_examenes);
    
	//2. Ordeno vector por burbujeo optimizado
	int q = 0;
    bool cambio;
    Dato aux;

    do {
        q++;
        cambio = false;

        for (int r = 0; r < cant - q; r++) { 

             if (vector[r].legajo > vector[r+1].legajo  ) {
                				
				aux = vector[r];
                vector[r] = vector[r+1];
				vector[r+1] = aux;
				
                cambio = true;
            } 
        }
    } while (cambio == true);
    
	/* Para testear: mostrar vector 
    for (int i = 0; i < cant; i++) {
    	cout << vector[i].legajo << " "<< vector[i].nota << endl;
	} 	*/
	//return cant;
}

void mostrar(char nomArch[])
{
    FILE * arch=fopen(nomArch,"rb");
    Dato reg;
    fread(&reg,sizeof(Dato),1,arch);
    while(!feof(arch))
    {
        cout<<"Legajo "<<reg.legajo;
        cout<<"  Nota "<<reg.nota<<endl;
        fread(&reg,sizeof(Dato),1,arch);

    }
    fclose(arch);
}

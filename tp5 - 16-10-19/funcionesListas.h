
template<typename T> struct Nodo
{
   T info;
   Nodo<T>* sig;
};

template<typename T> void insertar(Nodo<T>* & l,T dato,int(*criterio)(T,T))
{
   Nodo<T>*n=new Nodo<T>;
   n->info=dato;
   Nodo<T>*r=l;
   Nodo<T>*ant;
   while(r!= NULL && criterio(r->info,dato)<0)
   {
      ant=r;
      r=r->sig;
   }
   n->sig=r;
   if(r==l)
      l=n;
   else
      ant->sig=n;
}

template<typename T> void listar(Nodo<T>* l, void (*mostrar)(T))
{
   Nodo<T>*r=l;
   while(r!=NULL)
   {
      mostrar(r->info);
      r=r->sig;
   }
}
template<typename T> Nodo<T>* buscarInser(Nodo<T>* &l, T dato, int(*criterio)(T,T)){
  Nodo<T>*r=l;
  Nodo<T>*ant;
   while(r!=NULL && criterio(r->info,dato)<0)
      {
         ant=r;
         r=r->sig;
      }
   if(r!=NULL && criterio(r->info,dato)==0)
      return r;
   else
   {
      Nodo<T>*n=new Nodo<T>;
      n->info=dato;
      n->sig=r;
      if(r==l)
          l=n;
      else
         ant->sig=n;
       return n;
   }
 }

template<typename T,typename K> Nodo<T>* buscar(Nodo<T>* l, K dato, int(*criterio)(T,K)){
  Nodo<T>*r=l;
  Nodo<T>*ant;
   while(r!=NULL && criterio(r->info,dato)<0)
      {
         ant=r;
         r=r->sig;
      }
   if(r!=NULL && criterio(r->info,dato)==0)
      return r;
   else
      return NULL;
}

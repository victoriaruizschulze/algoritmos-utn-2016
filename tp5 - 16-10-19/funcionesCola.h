template<typename T> struct NodoC{
   T info;
   NodoC<T>* sig;
};

template<typename T> void encolar(NodoC<T> * &pri,NodoC<T> * & ult,T dato)
{
    NodoC<T> *p;
    p=new NodoC<T>;
    p->info=dato;
    p->sig=NULL;
    if(vacia(pri,ult))
        pri=p;
    else
        ult->sig=p;
    ult=p;
}

template<typename T> T desencolar(NodoC<T> * &pri,NodoC<T> * & ult)
{
    T dato;
    NodoC<T>*p=pri;
    dato=p->info;
    pri=p->sig;
    if(pri==NULL)
        ult=NULL;
    delete p;
    return dato;
}

template<typename T> bool vacia(NodoC<T> * pri,NodoC<T> * ult)
{
    /*if(pri==NULL && ult==NULL)
        return true;
    else
        return false;*/
        return pri==NULL && ult==NULL;
}
template<typename T> void listarC(NodoC<T>* l, void (*mostrar)(T))
{
   NodoC<T>*r=l;
   while(r!=NULL)
   {
      mostrar(r->info);
      r=r->sig;
   }
}


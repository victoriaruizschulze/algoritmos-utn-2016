#include <iostream>
#include <stdio.h>
#include <cstdlib>
#include <time.h>
#include <conio.h> //para ingresar el char al final
#include "funcionesListas.h"
#include "funcionesCola.h"

using namespace std;

struct Pedido {
	int cliente;
	int zona;
	int problema;	
};
struct Reparacion {
	int cliente;
	int problema;
};
struct Zona {
	int cod;
	Nodo<Reparacion>* sublista;
};

//ESTRUCTURAS ENLAZADAS
NodoC<Pedido>* cola_pedidos_p = NULL;
NodoC<Pedido>* cola_pedidos_u = NULL;
Nodo<Zona>* lista_zonas = NULL;
//Nodo<Reparacion>* lista_reparaciones = NULL;

void ingresar_pedido( NodoC<Pedido>* &cola_pedidos_p, NodoC<Pedido>* &cola_pedidos_u);
void preparar_pedidos(int cant, NodoC<Pedido>* &cola_pedidos_p, NodoC<Pedido>* &cola_pedidos_u, Nodo<Zona>* &lista_zonas);
void generacion_pedidos(NodoC<Pedido>* &cola_pedidos_p, NodoC<Pedido>* &cola_pedidos_u);
int crit_zonas(Zona aComparar, Zona buscada);
int crit_reparaciones(Reparacion aComparar, Reparacion buscado);
void listar_por_zona(Nodo<Zona>* &lista_zonas);

int main() {
	
	int opcion_menu;
	int cant_pedidos = 2; 
	
	do {
		cout << "\n\nBUEN DIA! QUE DESEA HACER? \n";
		cout << "--------------------------------------------------------------------------\n";
		cout << "   1) Ingresar un nuevo pedido.\n";
		cout << "   2) Trampa: que los pedidos se ingresen solos!\n";
	    cout << "   3) Preparar pedidos para su reparacion.\n";
	    cout << "   4) Informar por zona pedidos de reparacion listos para atender.\n";
	    cout << "   5) Finalizar el programa\n";
		cout << "--------------------------------------------------------------------------\n";
		cout << "Su eleccion: ";
		cin >> opcion_menu; //Validar?
		cout << endl;


		switch(opcion_menu) {
			case 1:
				ingresar_pedido(cola_pedidos_p, cola_pedidos_u);
				break;
			case 2: 
				generacion_pedidos(cola_pedidos_p, cola_pedidos_u);
				break;
			case 3: 
				cout << endl << "Cuantos pedidos desea preparar? ";
				do {cin >> cant_pedidos;} while (cant_pedidos < 0); // asegurarse de que sea un numero entero positivo
				preparar_pedidos(cant_pedidos, cola_pedidos_p, cola_pedidos_u, lista_zonas);
				break;
			case 4: 
				listar_por_zona(lista_zonas);
				break;
			case 5:
				cout << "Muchas gracias por utilizar este programa. Adios!" << endl;
				break;
			default:
				cout << "Opcion no valida.\n";
				break;
		}
		
	} while (opcion_menu != 5);
	    
    getch();
    return 0;
}
void mostrarP(Pedido a) {
   cout<<"Pedido: Cliente "<< a.cliente << " - Zona: " << a.zona << " - Problema " << a.problema <<endl;
} 
void mostrarR(Reparacion a) {
   cout<<"Reparacion: Cliente "<< a.cliente << " - Problema " << a.problema <<endl;
} 
void mostrarZ(Zona a) {
	cout << "ZONA " << a.cod << " -------------------------------" << endl;
	listar<Reparacion>(a.sublista, mostrarR);
	cout << endl;
}
int crit_zonas(Zona aComparar, Zona buscada) {
	return aComparar.cod - buscada.cod;
		//if (buscada.cod == aCompara.cod) {return 0;} else {return -1;}
}
int crit_reparaciones(Reparacion aComparar, Reparacion buscado) {
	//cout << "crit_reparaciones: " << aComparar.cliente <<"-"<<buscado.cliente << " = "<< (aComparar.cliente - buscado.cliente)<<endl;
	return aComparar.cliente - buscado.cliente;
}
void ingresar_pedido(NodoC<Pedido>* &cola_pedidos_p, NodoC<Pedido>* &cola_pedidos_u) {
	
	Pedido pedido_nuevo;
	int otro; 
	
	do  {	
		cout << "Nuevo pedido de reparacion: \n" ;
		cout << "Ingrese el numero de cliente: " ;
		cin >> pedido_nuevo.cliente;
		cout << "\nIngrese el codigo de zona: " ;
		cin >> pedido_nuevo.zona;
		cout << "\nIngrese el codigo de problema: " ;
		cin >> pedido_nuevo.problema;
		cout << endl;
		cout << "Desea ingresar otro pedido (1 = si, 0 = no)?";
		cin >> otro;
		encolar(cola_pedidos_p,cola_pedidos_u, pedido_nuevo);
	} while (otro == 1);
	
	listarC<Pedido>(cola_pedidos_p,mostrarP);
}
void generacion_pedidos(NodoC<Pedido>* &cola_pedidos_p, NodoC<Pedido>* &cola_pedidos_u) {
	
	int cant;
	Pedido pedido_nuevo;
	int otro; 

	cout << "Cuantos pedidos quiere generar? ";
	cin >> cant;
	cout << endl;

	if (cant > 0 ) {

		srand (time(NULL));
		for (int i = 0; i < cant; i++) {	
			pedido_nuevo.cliente =  1500 + (rand() % 20 + 1) ;  // total de clientes: 20
			pedido_nuevo.zona = rand() % 10 + 1; // entre 1 y 10
			pedido_nuevo.problema = rand() % 100 + 1; // entre 1 y 100
			
			encolar(cola_pedidos_p,cola_pedidos_u, pedido_nuevo);
		};
	} else {
		cout << "No agrego pedidos nuevos. \n";
	}
	
	cout << "Pedidos en la cola actual: \n";
	cout << "-------------------------- \n";
	listarC<Pedido>(cola_pedidos_p,mostrarP);

}
void preparar_pedidos(int cant, NodoC<Pedido>* &cola_pedidos_p, NodoC<Pedido>* &cola_pedidos_u, Nodo<Zona>* &lista_zonas) {
	Pedido pedido;
	Reparacion reparacion;
	Zona 	zona; 
	Nodo<Zona>* zonaEncontrada;
	int cont = 0; 

	cout << "Reparaciones listas para ser encaradas: \n";
	cout << "--------------------------------------- \n";

	while (cont < cant && cola_pedidos_p != NULL) {
		pedido = desencolar<Pedido>(cola_pedidos_p,cola_pedidos_u); //desencolar 1 pedido. 
		zona.cod = pedido.zona; 
		zona.sublista = NULL;
		reparacion.cliente = pedido.cliente;
		reparacion.problema = pedido.problema; 
		// buscar la zona del pedido en la lista zonas, 
			//si no existe, agregar zona (en orden!) y crear lista de reparaciones
		zonaEncontrada = buscarInser<Zona>(lista_zonas, zona, crit_zonas);

		//en la zona encontrada/agregada, ver si existe el pedido en la lista de reparaciones de la zona
		buscarInser<Reparacion>(zonaEncontrada->info.sublista, reparacion, crit_reparaciones);
		mostrarR(reparacion);

		cont++;
	}
	
	cout << "\nPedidos en la cola actual: \n";
	cout << "-------------------------- \n";
	listarC<Pedido>(cola_pedidos_p,mostrarP);
	
}
void listar_por_zona(Nodo<Zona>* &lista_zonas) {
	listar<Zona>(lista_zonas, mostrarZ);
}
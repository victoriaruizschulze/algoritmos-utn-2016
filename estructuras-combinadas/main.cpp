//1: tengo archivo con 200 regs de: 
#include <string.h>
#include <iostream>
#include <stdio.h>

using namespace std;
struct Pedido {
	int codigo;
	int cantidad;
};
struct Reg_plato {
	unsigned int codigo; 
	//string desc;
	float precio; 
};

struct Plato {
	string desc; 
	float precio; 
};
struct NodoP {
	NodoP* sig;
	Pedido pedido;
};
struct Factura {
	int fecha; 
	int mozo; 
	int factura; 
	int mesa; 
	int total;
};

void generar_vector_platos();
void servir_plato(int, int);
void finalizar_cuenta(int);
NodoP* buscarInsertarPedido(NodoP*&, int);

Plato v_platos[200]; 
NodoP* v_pedidos[48];

int factura_ini = 1; 
int fecha = 161114; //AAMMDD

FILE* facturacion_dia = fopen("facturacion.bin", "rb+");


int main() {

	char accion; 
	int mesa; 

	
	generar_vector_platos();
	

	for (int i = 0; i < 48; i++) {
		v_pedidos[i] = NULL;
	}
	
	do {
		cout << "Que accion desea hacer (ABF)?"; 
		cin >> accion;

		switch (accion) {
			case 'A': 
				cout << "Ingrese la mesa: ";
				cin >> mesa;
				if (mesa > 0 && mesa < 49) {
					servir_plato(1, mesa);
				}
				break;
			case 'B': 
				cout << "Ingrese la mesa: ";
				cin >> mesa;
				if (mesa > 0 && mesa < 49) {
					servir_plato(-1, mesa);
				}
				break;
			case 'F': 
				cout << "Ingrese la mesa: ";
				cin >> mesa;
				finalizar_cuenta(mesa);
				break;
			case 'C': 
				break;
			default: 
				cout << "Error! Intente nuevamente. ";
				break;

		}

	} while (accion != 'C');

	return 0;
}

void generar_vector_platos() {
	cout << "Hola";
	FILE* archivo = fopen("platos.bin", "rb");
	Reg_plato plato;
	fread(&plato, sizeof(Reg_plato), 1, archivo);

	while (!feof(archivo)) {

		//v_platos[plato.codigo-1].desc = plato.desc;
		v_platos[plato.codigo-1].precio = plato.precio;

		fread(&plato, sizeof(Reg_plato), 1, archivo);
	}
}
void servir_plato(int cantidad, int mesa) {	
	int codigo; 
	cout << "Ingrese el codigo de plato";
	cin >> codigo;

	NodoP* tipo_plato = buscarInsertarPedido(v_pedidos[mesa-1], codigo);
	
	tipo_plato->pedido.cantidad = tipo_plato->pedido.cantidad + cantidad;

	if (tipo_plato->pedido.cantidad < 0) {
		tipo_plato->pedido.cantidad = 0; 
		cout << "Error: devolvió un plato que nunca pidió. ";
	}

};
void finalizar_cuenta(int mesa){
	int mozo; 
	float importe; 

	cout << "Ingrese el mozo: ";
	cin >> mozo;
	
	float total; 
	factura_ini++;

	

	while (v_pedidos[mesa-1] != NULL) {
		importe = v_pedidos[mesa-1]->pedido.cantidad *  v_platos[(v_pedidos[mesa-1]->pedido.codigo) - 1].precio;
		cout << "Cantidad: " << v_pedidos[mesa-1]->pedido.cantidad << endl; 
		cout << "Precio unitario: " << v_platos[(v_pedidos[mesa-1]->pedido.codigo) - 1].precio << endl;  
		cout << "Desc no importa" << endl; 
		cout << "Importe: " << importe << endl << endl;

		total = total + importe; 
	}

	Factura* f = new Factura;
	f->total = importe; 
	f->fecha = fecha; 
	f->factura = factura_ini; 
	f->mozo = mozo; 

	fwrite(&f, sizeof(Factura), 1, facturacion_dia);
	delete f;

	cout << "Fecha: "<<fecha << endl<<"Factura no: "<< factura_ini << endl << "Mozo: " << mozo;
	cout << "Total: " << importe << endl;

	v_pedidos[mesa-1] = NULL;
};
void finalizar_dia(){

	//fclose(facturacion_dia);
	fseek(facturacion_dia, 0, SEEK_SET);
	
	Factura f; 

	fread(&f, sizeof(Factura), 1, )

}

NodoP* buscarInsertarPedido(NodoP* &lista, int codigo) {
	NodoP* aux = lista; 
	NodoP* ant; 


	while(aux != NULL && aux->pedido.codigo < codigo ) {
		ant->sig = aux;
		aux = aux->sig;
	}

	//2 casos posibles: lo encontró, no lo encontró

	if (aux != NULL && aux->pedido.codigo == codigo) {
		//lo encontró. 2 casos posibles: es el primero, está en el medio o en el final
		return aux;
	} else {
		//Aux = null O pedido.cod > codigo
		NodoP* nuevo = new NodoP;
		nuevo->pedido.codigo = codigo; 
		nuevo->pedido.cantidad = 0;
		nuevo->sig = aux;

		if (aux == lista) {
			lista = nuevo;
		} else {
			ant->sig = nuevo; 
		}
		return nuevo;
	}

}